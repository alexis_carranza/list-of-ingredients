# README

Sample application that lists alcoholic beverage and the ingredients required to make it.

Technologies used:
1. Ruby on Rails for backend.
2. React for front-end

Notes:
- The backend server(RoR) serves the front-end server(React) through an API.

Live Demo:
* https://shielded-taiga-58567.herokuapp.com/
